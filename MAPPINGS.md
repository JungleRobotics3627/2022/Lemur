# Mapping
The Jungle 2022 robot is equipped with a smart controller detection system. 
On enable, the robot will scan the connected controllers and decide the best controller configuration.

# Configurations
There are currently three supported configurations which are as follows: Single Gamepad, Single Joystick, and Dual Joystick.

## Single Gamepad
- *Left Bumper*: **Gear Shift Down**
- *Left Trigger*: **Lower Conveyor**
- *Right Bumper*: **Gear Shift Up**
- *Right Trigger*: **Upper Conveyor**
- *D-Pad*: **N/A**
- *B; Circle*: **Upper Hooks Toggle**
- *Y; Triangle*: **N/A**
- *X; Square*: **Lower Hooks Toggle**
- *A; X*: **Conveyor Reverse**
- *Left Joystick X*: **Robot Turn**
- *Left Joystick Y*: **Robot Throttle**
- *Left Joystick Button*: **N/A**
- *Right Joystick X*: **N/A**
- *Right Joystick Y*: **Hook Arm**
- *Right Joystick Button*: **N/A**

## Single Joystick
***Not Functional, DO NOT USE***

## Double Joystick
***Not Functional, DO NOT USE***