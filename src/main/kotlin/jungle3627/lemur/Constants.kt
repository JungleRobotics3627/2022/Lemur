package jungle3627.lemur

import jungle3627.lemur.configs.Gear

/**
 * Robot-wide numerical and boolean constants
 */
object Constants {
    /**
     * IDs of the CAN TalonSRX drive motors
     */
    object Drive {
        const val maxSpeed: Double = 3.5
        const val maxUserAcceleration: Double = 5.0
        const val maxAcceleration: Double = 5.0
        const val filerTimeConstant: Double = 0.3
        const val encoderDistPerPulse: Double = 0.001329941

        object FeedForward {
            const val ks: Double = 0.83955
            const val kv: Double = 3.1182
            const val ka: Double = 0.79838
        }

        object PID {
            const val P: Double = 6.0
            const val I: Double = 0.0
            const val D: Double = 0.5

            const val integratorRange: Double = 5.0
        }

        object Ports {
            object Motors {
                const val rightFront: Int = 3
                const val rightRear: Int = 2
                const val leftFront: Int = 1
                const val leftRear: Int = 0

                object Invert {
                    const val rightFront: Boolean = true
                    const val rightRear: Boolean = true
                    const val leftFront: Boolean = false
                    const val leftRear: Boolean = false
                }
            }

            object Encoders {
                const val leftA: Int = 2
                const val leftB: Int = 3
                const val rightA: Int = 0
                const val rightB: Int = 1

                object Invert {
                    const val left: Boolean = true
                    const val right: Boolean = false
                }
            }
        }
    }

    object Climb {
        const val coolDownTime: Double = 25.0

        object ServoLimits {
            const val leftFrontRelease: Double = 0.0
            const val leftFrontLock: Double = 180.0

            const val leftRearRelease: Double = 0.0
            const val leftRearLock: Double = 180.0

            const val rightFrontRelease: Double = 0.0
            const val rightFrontLock: Double = 180.0

            const val rightRearRelease: Double = 0.0
            const val rightRearLock: Double = 180.0
        }

        object Ports {
            const val fanPort: Int = 0

            object Servos {
                const val leftFront: Int = 8
                const val leftRear: Int = 9
                const val rightFront = 0
                const val rightRear = 1
            }

            object Motors {
                const val rightFront: Int = 9
                const val rightRear: Int = 8
                const val leftFront: Int = 7
                const val leftRear: Int = 6

                object Invert {
                    const val rightFront: Boolean = false
                    const val rightRear: Boolean = false
                    const val leftFront: Boolean = true
                    const val leftRear: Boolean = false
                }
            }
        }
    }

    object Conveyor {
        object Ports {
            object Motors {
                const val upper: Int = 5
                const val lower: Int = 4

                object Invert {
                    const val upper: Boolean = true
                    const val lower: Boolean = true
                }
            }
        }
    }

    /**
     * All constants pertaining to human input devices
     */
    object InputParameters {
        const val sensitivityPower: Double = 1.0

        /**
         * The time for which the enable condition must be met to exit climb lockout mode
         */
        const val climbEnableTime: Double = 1.0

        /**
         * Multipliers that set max speeds for the virtual gears
         *
         * All values must be in range `(0.0..1.0]`
         */
        val gears: List<Gear> = listOf(
            Gear(0.125, 0.125),
            Gear(0.25, 0.25),
            Gear(0.5, 0.35),
            Gear(1.0, 0.5),
        )

        /**
         * The IDs of axes and buttons on various controllers
         */
        object Channels {
            object LogitechAttack3 {
                const val xAxis: Int = 0
                const val yAxis: Int = 1
                const val upButton: Int = 3
                const val downButton: Int = 2
            }

            object PlayStation4 {
                const val deadZone: Double = 0.035

                object RightJoystick {
                    const val xAxis: Int = 4
                    const val yAxis: Int = 5
                    const val button: Int = 10
                }

                object LeftJoystick {
                    const val xAxis: Int = 0
                    const val yAxis: Int = 1
                    const val button: Int = 9
                }

                const val yButton: Int = 4
                const val bButton: Int = 2
                const val aButton: Int = 1
                const val xButton: Int = 3
                const val rightBumper: Int = 6
                const val leftBumper: Int = 5

                const val rightTrigger: Int = 3
                const val leftTrigger: Int = 2
            }

            object Xbox360 {
                const val deadZone: Double = 0.035

                object RightJoystick {
                    const val xAxis: Int = 4
                    const val yAxis: Int = 5
                    const val button: Int = 10
                }

                object LeftJoystick {
                    const val xAxis: Int = 0
                    const val yAxis: Int = 1
                    const val button: Int = 9
                }

                const val yButton: Int = 4
                const val bButton: Int = 2
                const val aButton: Int = 1
                const val xButton: Int = 3
                const val rightBumper: Int = 6
                const val leftBumper: Int = 5

                const val rightTrigger: Int = 3
                const val leftTrigger: Int = 2
            }
        }
    }

    object AutoParameters {
        const val conveyorTime: Double = 3.0

        const val driveDistance: Double = 2.3
        const val driveSpeed: Double = 0.35
    }
}
