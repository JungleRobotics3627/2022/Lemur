package jungle3627.lemur.commands

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup

import jungle3627.lemur.Constants.AutoParameters

class AutoCommand(shoot: Boolean, drive: Boolean): SequentialCommandGroup() {
    init {
        if(shoot) {
            addCommands(
                TimedConveyorCommand(AutoParameters.conveyorTime)
            )
        }

        if(drive) {
            addCommands(
                DistanceDriveCommand(AutoParameters.driveDistance, AutoParameters.driveSpeed, false)
            )
        }
    }
}