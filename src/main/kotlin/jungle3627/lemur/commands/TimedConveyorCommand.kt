package jungle3627.lemur.commands

import edu.wpi.first.util.WPIUtilJNI
import edu.wpi.first.wpilibj2.command.CommandBase
import jungle3627.lemur.subsystems.ConveyorSubsystem

class TimedConveyorCommand(private val timeToRun: Double): CommandBase() {
    private var startTime = 0.0
    private var finished = false

    init {
        addRequirements(ConveyorSubsystem)
    }

    override fun initialize() {
        println("TimedConveyorCommand.initialize()")
        startTime = seconds
        finished = false
    }

    override fun execute() {
        if(seconds - startTime < timeToRun) {
            ConveyorSubsystem.setUpperConveyor(1.0)
            ConveyorSubsystem.setLowerConveyor(1.0)
        } else {
            ConveyorSubsystem.setUpperConveyor(0.0)
            ConveyorSubsystem.setLowerConveyor(0.0)
            finished = true
        }
    }

    override fun isFinished(): Boolean {
        return finished
    }

    override fun end(interrupted: Boolean) {
        ConveyorSubsystem.stop()
    }

    private val seconds: Double
        get() = WPIUtilJNI.now() * 1e-6
}