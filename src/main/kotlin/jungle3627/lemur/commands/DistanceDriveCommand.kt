package jungle3627.lemur.commands

import edu.wpi.first.wpilibj2.command.CommandBase
import jungle3627.lemur.subsystems.DriveSubsystem

class DistanceDriveCommand(private val distance: Double, private val speed: Double, private val reverse: Boolean): CommandBase() {
    private var finished = false
    private var startingDist = 0.0

    init {
        addRequirements(DriveSubsystem)
    }

    override fun initialize() {
        println("DistanceDriveCommand.initialize()")
        startingDist = DriveSubsystem.getDistanceTraveled()
        finished = false

        if(!reverse)
            DriveSubsystem.drive(speed, speed)
        else
            DriveSubsystem.drive(-speed, -speed)
    }

    override fun execute() {
        if (!reverse) {
            if (DriveSubsystem.getDistanceTraveled() - startingDist >= distance) {
                DriveSubsystem.stop()
                finished = true
            }
        } else {
            if (DriveSubsystem.getDistanceTraveled() - startingDist <= -distance) {
                DriveSubsystem.stop()
                finished = true
            }
        }
    }

    override fun isFinished(): Boolean {
        return finished
    }

    override fun end(interrupted: Boolean) {
        DriveSubsystem.stop()
    }
}