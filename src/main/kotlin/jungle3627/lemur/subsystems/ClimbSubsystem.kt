package jungle3627.lemur.subsystems

import com.ctre.phoenix.motorcontrol.InvertType
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX
import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup
import edu.wpi.first.wpilibj2.command.SubsystemBase
import edu.wpi.first.util.WPIUtilJNI.now
import edu.wpi.first.wpilibj.Relay
import edu.wpi.first.wpilibj.Servo

import jungle3627.lemur.Constants.Climb
import kotlin.math.abs

object ClimbSubsystem : SubsystemBase() {
    private val rightFrontMotor = WPI_VictorSPX(Climb.Ports.Motors.rightFront)
        .run { if (Climb.Ports.Motors.Invert.rightFront) setInverted(InvertType.InvertMotorOutput); this }

    private val rightRearMotor = WPI_VictorSPX(Climb.Ports.Motors.rightRear)
        .run { if (Climb.Ports.Motors.Invert.rightRear) setInverted(InvertType.InvertMotorOutput); this }

    private val leftFrontMotor = WPI_VictorSPX(Climb.Ports.Motors.leftFront)
        .run { if (Climb.Ports.Motors.Invert.leftFront) setInverted(InvertType.InvertMotorOutput); this }

    private val leftRearMotor = WPI_VictorSPX(Climb.Ports.Motors.leftRear)
        .run { if (Climb.Ports.Motors.Invert.leftRear) setInverted(InvertType.InvertMotorOutput); this }

    private val climbMotors = MotorControllerGroup(rightFrontMotor, rightRearMotor, leftFrontMotor, leftRearMotor)

    private val leftFrontServo = Servo(Climb.Ports.Servos.leftFront)
    private val leftRearServo = Servo(Climb.Ports.Servos.leftRear)
    private val rightFrontServo = Servo(Climb.Ports.Servos.rightFront)
    private val rightRearServo = Servo(Climb.Ports.Servos.rightRear)

    private var frontHooksLocked: Boolean = false
    private var rearHooksLocked: Boolean = false

    private val fans = Relay(Climb.Ports.fanPort)

    var climbEnabled: Boolean = false

    private val seconds: Double
        get() = now() * 1e-6

    private var lastArmMovement: Double = -100.0

    init {
        climbEnabled = true
        releaseRearHooks()
        releaseFrontHooks()
        climbEnabled = false
    }

    /**
     * Rotates the climbing arms
     *
     * @param value The speed to rotate the arms (positive climbs up)
     */
    fun rotateArms(value: Double) {
        if(climbEnabled) {
            if(abs(value) > 0.1) {
                lastArmMovement = seconds
            }

            climbMotors.set(value)
        }
    }

    fun releaseFrontHooks() {
        if(climbEnabled) {
            frontHooksLocked = false
            leftFrontServo.angle = Climb.ServoLimits.leftFrontRelease
            rightFrontServo.angle = Climb.ServoLimits.rightFrontRelease
            println("ClimbSubsystem: Releasing front hooks.")
        }
    }

    fun lockFrontHooks() {
        if(climbEnabled) {
            frontHooksLocked = true
            leftFrontServo.angle = Climb.ServoLimits.leftFrontLock
            rightFrontServo.angle = Climb.ServoLimits.rightFrontLock
            println("ClimbSubsystem: Locking front hooks.")
        }
    }

    fun releaseRearHooks() {
        if(climbEnabled) {
            rearHooksLocked = false
            leftRearServo.angle = Climb.ServoLimits.leftRearRelease
            rightRearServo.angle = Climb.ServoLimits.rightRearRelease
            println("ClimbSubsystem: Releasing rear hooks.")
        }
    }

    fun lockRearHooks() {
        if(climbEnabled) {
            rearHooksLocked = true
            leftRearServo.angle = Climb.ServoLimits.leftRearLock
            rightRearServo.angle = Climb.ServoLimits.rightRearLock
            println("ClimbSubsystem: Locking rear hooks.")
        }
    }

    fun isFrontHooksLocked(): Boolean {
        return frontHooksLocked
    }

    fun isRearHooksLocked(): Boolean {
        return rearHooksLocked
    }

    fun isFrontHooksReleased(): Boolean {
        return !frontHooksLocked
    }

    fun isRearHooksReleased(): Boolean {
        return !rearHooksLocked
    }

    override fun periodic() {
        if(seconds - lastArmMovement <= Climb.coolDownTime) {
            fans.set(Relay.Value.kForward)
        } else {
            fans.set(Relay.Value.kOff)
        }
    }

    fun stop() {
        println("ClimbSubsystem.stop()")
        climbMotors.stopMotor()
    }
}