package jungle3627.lemur.subsystems

import edu.wpi.first.util.WPIUtilJNI
import edu.wpi.first.wpilibj.GenericHID
import edu.wpi.first.wpilibj2.command.SubsystemBase

import jungle3627.lemur.Constants.InputParameters
import jungle3627.lemur.hid.devices.gamepads.*
import jungle3627.lemur.hid.devices.joysticks.*
import jungle3627.lemur.hid.setups.*
import kotlin.math.pow
import kotlin.math.withSign

/**
 * Abstraction of user input
 */
object InputSubsystem : SubsystemBase() {
    /**
     * List of the 6 available human interfaces
     */
    private val availableControllers = listOf(
        GenericHID(0),
        GenericHID(1),
        GenericHID(2),
        GenericHID(3),
        GenericHID(4),
        GenericHID(5),
    )
        get() = field.filter { it.isConnected } // only want the connected controllers when accessed

    /**
     * The catch-all controller abstraction
     */
    private lateinit var controller: HIDSetup

    /**
     * The speed gear the robot is currently using
     *
     * ```
     * Min (12.5%)
     * Low (25%)
     * Mid (50%)
     * Top (100%)
     * ```
     *
     * @see InputParameters.gears
     */
    private var gear: Int = 1
        get() {
            var newGear: Int = field

            if (controller.isUpShiftPressed) newGear += 1
            if (controller.isDownShiftPressed) newGear -= 1

            field = newGear.coerceIn(0 until InputParameters.gears.size)

            return field
        }

    var climbEnabled: Boolean = false
        private set

    private var lastClimbEnableDown: Double = -1.0
    private var climbEnableDown: Boolean = false

    /**
     * Gets the throttle based on the joystick position(s)
     */
    val throttle: Double
        get() {
//            updateSlewRates()
            val postSense = controller.forward.pow(InputParameters.sensitivityPower).withSign(controller.forward)
//            val postSlew = throttleSlew.compute(postSense)

            return postSense * InputParameters.gears[gear].throttleMultiplier
        }


    /**
     * Gets the turn based on the joystick position(s)
     */
    val turn: Double
        get() {
//            updateSlewRates()
            val postSense = controller.turn.pow(InputParameters.sensitivityPower).withSign(controller.turn)
//            val postSlew = turnSlew.compute(postSense)

            return postSense * InputParameters.gears[gear].turnMultiplier
        }

    val climb: Double
        get() {
            return controller.climb
        }

    val upperConveyor: Double
        get() = controller.upperConveyor

    val lowerConveyor: Double
        get() = controller.lowerConveyor

    val isFrontHooksPressed: Boolean
        get() {
            return controller.isFrontHooksPressed
        }

    val isRearHooksPressed: Boolean
        get() {
            return controller.isRearHooksPressed
        }

    /**
     * Determine input configuration
     *
     * Priorities
     *  1. DoubleJoystick
     *  2. XGamepad (Xbox and PS4 have equal priority)
     *  3. SingleJoystick
     *
     *  When multiple of the highest available priority configurations are available, first is used
     *
     *  Example USB Order:
     *  ```
     *  0. Joystick
     *  1. PS4
     *  2. Xbox
     *  ```
     *
     *  In this example, it will use the PS4 controller in slot `1`
     *
     *  If no known setup is found, it will use [Unrecognized] and all controls will return default values
     */
    fun configure() {
        // get separate lists of the joysticks and the gamepads
        val joysticks: List<GenericHID> = availableControllers.filter { it.type == GenericHID.HIDType.kXInputFlightStick || it.type == GenericHID.HIDType.kHIDJoystick }
        val gamepads: List<GenericHID> = availableControllers.filter {
                it.type == GenericHID.HIDType.kXInputGamepad ||
                it.type == GenericHID.HIDType.kHIDGamepad ||
                it.name.contains("xbox", true) ||
                it.name.contains("x-box", true)
            }

        // use priority chain to select the correct input mode and pick the appropriate devices
        controller = if (joysticks.size >= 2) {
            println("InputSubsystem: Config set to DoubleJoystick")
            DoubleJoystick(
                leftJoystick = JungleLogitechAttack3(joysticks[0].port),
                rightJoystick = JungleLogitechAttack3(joysticks[1].port),
            )
        } else if (gamepads.size >= 1) {
            val gamepad = gamepads[0]

            // xbox and ps4 gamepads require different implementations, so we must determine what type of gamepad it is
            if (gamepad.name.contains("xbox", true) || gamepad.name.contains("x-box", true)) {
                println("InputSubsystem: Config set to SingleGamepad (XBox).")
                SingleGamepad(gamepad = JungleXbox(gamepad.port))
            } else {
                println("InputSubsystem: Config set to SingleGamepad (PS4).")
                SingleGamepad(gamepad = JunglePS4(gamepad.port))
            }
        } else if (joysticks.size == 1) {
            println("InputSubsystem: Config set to SingleJoystick")
            SingleJoystick(joystick = JungleLogitechAttack3(joysticks[0].port))
        } else {
            println("InputSubsystem: Config not set. No valid config detected.")
            Unrecognized() // no known setup was found
        }
    }

    override fun periodic() {
        if(controller.isClimbEnableDown) {
            if(lastClimbEnableDown < 0) {
                lastClimbEnableDown = seconds
            }
        } else {
            lastClimbEnableDown = -1.0
        }

        if( lastClimbEnableDown >= 0 && seconds - lastClimbEnableDown > InputParameters.climbEnableTime) {
            climbEnableDown = true
            controller.rumble = 1.0
        }

        if(controller.isClimbEnableUp && climbEnableDown) {
            climbEnabled = true
            climbEnableDown = false
            controller.rumble = 0.0
            println("InputSubsystem: Climb unlocked.")
        }
    }

    private val seconds: Double
        get() = WPIUtilJNI.now() * 1e-6
}