package jungle3627.lemur.subsystems

import com.ctre.phoenix.motorcontrol.InvertType
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX
import edu.wpi.first.wpilibj2.command.SubsystemBase
import jungle3627.lemur.Constants.Conveyor

object ConveyorSubsystem : SubsystemBase() {
    private val upperMotor = WPI_VictorSPX(Conveyor.Ports.Motors.upper)
        .run { if (Conveyor.Ports.Motors.Invert.upper) setInverted(InvertType.InvertMotorOutput); this }

    private val lowerMotor = WPI_VictorSPX(Conveyor.Ports.Motors.lower)
        .run { if (Conveyor.Ports.Motors.Invert.lower) setInverted(InvertType.InvertMotorOutput); this }

    fun setUpperConveyor(value: Double) {
        upperMotor.set(value)
    }

    fun setLowerConveyor(value: Double) {
        lowerMotor.set(value)
    }

    fun stop() {
        println("ConveyorSystems.stop()")
        upperMotor.stopMotor()
        lowerMotor.stopMotor()
    }
}