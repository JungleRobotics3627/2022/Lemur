package jungle3627.lemur.subsystems

import com.ctre.phoenix.motorcontrol.InvertType
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX
import com.kauailabs.navx.frc.AHRS
import edu.wpi.first.math.filter.LinearFilter
import edu.wpi.first.math.geometry.Pose2d
import edu.wpi.first.math.geometry.Rotation2d
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry
import edu.wpi.first.wpilibj.Encoder
import edu.wpi.first.wpilibj.SerialPort
import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup
import edu.wpi.first.wpilibj2.command.SubsystemBase

import jungle3627.lemur.Constants.Drive
import jungle3627.lemur.configs.VelocityPIDConfig

object DriveSubsystem : SubsystemBase() {
    private val rightFrontMotor = WPI_TalonSRX(Drive.Ports.Motors.rightFront)
        .run { if (Drive.Ports.Motors.Invert.rightFront) setInverted(InvertType.InvertMotorOutput); this }

    private val rightRearMotor = WPI_TalonSRX(Drive.Ports.Motors.rightRear)
        .run { if (Drive.Ports.Motors.Invert.rightRear) setInverted(InvertType.InvertMotorOutput); this }

    private val leftFrontMotor = WPI_TalonSRX(Drive.Ports.Motors.leftFront)
        .run { if (Drive.Ports.Motors.Invert.leftFront) setInverted(InvertType.InvertMotorOutput); this }

    private val leftRearMotor = WPI_TalonSRX(Drive.Ports.Motors.leftRear)
        .run { if (Drive.Ports.Motors.Invert.leftRear) setInverted(InvertType.InvertMotorOutput); this }

    private val leftMotorControllers = MotorControllerGroup(leftFrontMotor, leftRearMotor)
    private val rightMotorControllers = MotorControllerGroup(rightFrontMotor, rightRearMotor)

    private val leftMotor = VelocityControlledMotor(
        leftMotorControllers,
        VelocityPIDConfig(
            Drive.PID.P,
            Drive.PID.I,
            Drive.PID.D,
            Drive.PID.integratorRange,
            Drive.FeedForward.ks,
            Drive.FeedForward.kv,
            Drive.FeedForward.ka
        ),
        Encoder(
            Drive.Ports.Encoders.leftA,
            Drive.Ports.Encoders.leftB,
            Drive.Ports.Encoders.Invert.left,
        ).run { distancePerPulse = Drive.encoderDistPerPulse; this },
        Drive.maxAcceleration,
        LinearFilter.singlePoleIIR(Drive.filerTimeConstant, 0.02)
    )

    private val rightMotor = VelocityControlledMotor(
        rightMotorControllers,
        VelocityPIDConfig(
            Drive.PID.P,
            Drive.PID.I,
            Drive.PID.D,
            Drive.PID.integratorRange,
            Drive.FeedForward.ks,
            Drive.FeedForward.kv,
            Drive.FeedForward.ka
        ),
        Encoder(
            Drive.Ports.Encoders.rightA,
            Drive.Ports.Encoders.rightB,
            Drive.Ports.Encoders.Invert.right,
        ).run { distancePerPulse = Drive.encoderDistPerPulse; this },
        Drive.maxAcceleration,
        LinearFilter.singlePoleIIR(Drive.filerTimeConstant, 0.02)
    )

    private val gyro = AHRS(SerialPort.Port.kMXP)
    private val odometer = DifferentialDriveOdometry(gyro.rotation2d, Pose2d(0.0, 0.0, Rotation2d(0.0, 1.0)))

    private var odometerInitialized = false

    init {
        gyro.calibrate()
    }

    /**
     * Drives the robot
     *
     * @param left the left speed
     * @param right the right speed
     */
    fun drive(left: Double, right: Double) {
        leftMotor.velocity = left
        rightMotor.velocity = right
    }

    override fun periodic() {
        if(!odometerInitialized && !gyro.isCalibrating) {
            odometer.resetPosition(Pose2d(0.0, 0.0, Rotation2d(0.0, 1.0)), gyro.rotation2d)
            leftMotor.resetEncoder()
            rightMotor.resetEncoder()
            odometerInitialized = true
        }

        odometer.update(gyro.rotation2d, leftMotor.distance, rightMotor.distance)
    }

    fun getDistanceTraveled(): Double {
        return odometer.poseMeters.y
    }

    fun stop() {
        println("DriveSubsystem.stop()")

        leftMotor.velocity = 0.0
        rightMotor.velocity = 0.0
    }
}