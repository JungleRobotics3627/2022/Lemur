package jungle3627.lemur.subsystems

import edu.wpi.first.math.controller.PIDController
import edu.wpi.first.math.controller.SimpleMotorFeedforward
import edu.wpi.first.math.filter.LinearFilter
import edu.wpi.first.math.filter.SlewRateLimiter
import edu.wpi.first.wpilibj.Encoder
import edu.wpi.first.wpilibj.motorcontrol.MotorController
import edu.wpi.first.wpilibj2.command.PIDSubsystem

import jungle3627.lemur.configs.VelocityPIDConfig

class VelocityControlledMotor (
    private val motor: MotorController,
    pidParameters: VelocityPIDConfig,
    private val encoder: Encoder,
    maxVelocity: Double? = null,
    private val filter: LinearFilter? = null
): PIDSubsystem(PIDController(pidParameters.kp, pidParameters.ki, pidParameters.kd)) {

    private val feedforward = SimpleMotorFeedforward(pidParameters.ks, pidParameters.kv, pidParameters.ka)

    private var setVelocity = 0.0
    private val slewRateLimiter: SlewRateLimiter? = maxVelocity?.let { SlewRateLimiter(it, 0.0) }

    init {
        controller.setIntegratorRange(-pidParameters.kIntegrationRange, pidParameters.kIntegrationRange)

        setpoint = 0.0
        enable()
    }

    override fun useOutput(output: Double, setpoint: Double) {
        motor.setVoltage(output + feedforward.calculate(setpoint))
    }

    override fun getMeasurement(): Double {
        return filter?.calculate(encoder.rate) ?: encoder.rate
    }

    override fun periodic() {
        setpoint = slewRateLimiter?.calculate(setVelocity) ?: velocity

        super.periodic()
    }

    var velocity: Double
        get() {
            return encoder.rate
        }
        set(value) {
            setVelocity = value
        }

    val distance: Double
        get() {
            return encoder.distance
        }

    fun resetEncoder() {
        encoder.reset()
    }
}