package jungle3627.lemur.hid.devices.gamepads

import edu.wpi.first.wpilibj.GenericHID
import jungle3627.lemur.Constants.InputParameters.Channels

class JunglePS4(port: Int) : JungleGamepad(port) {
    override val leftJoystickX: Double
        get() = deadZone(wpiDevice.getRawAxis(Channels.PlayStation4.LeftJoystick.xAxis), Channels.PlayStation4.deadZone)
    
    override val leftJoystickY: Double
        get() = deadZone(-wpiDevice.getRawAxis(Channels.PlayStation4.LeftJoystick.yAxis), Channels.PlayStation4.deadZone)
    
    override val isLeftJoystickPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.PlayStation4.LeftJoystick.button)
    
    override val rightJoystickX: Double
        get() = deadZone(wpiDevice.getRawAxis(Channels.PlayStation4.RightJoystick.xAxis), Channels.PlayStation4.deadZone)

    override val rightJoystickY: Double
        get() = deadZone(-wpiDevice.getRawAxis(Channels.PlayStation4.RightJoystick.yAxis), Channels.PlayStation4.deadZone)

    override val isRightJoystickPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.PlayStation4.RightJoystick.button)

    override val dPad: Int
        get() = wpiDevice.pov

    override val isTopButtonPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.PlayStation4.yButton)
    override val isTopButtonDown: Boolean
        get() = wpiDevice.getRawButton(Channels.PlayStation4.yButton)

    override val isRightButtonPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.PlayStation4.bButton)
    override val isRightButtonDown: Boolean
        get() = wpiDevice.getRawButton(Channels.PlayStation4.bButton)

    override val isBottomButtonPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.PlayStation4.aButton)
    override val isBottomButtonDown: Boolean
        get() = wpiDevice.getRawButton(Channels.PlayStation4.aButton)

    override val isLeftButtonPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.PlayStation4.xButton)
    override val isLeftButtonDown: Boolean
        get() = wpiDevice.getRawButton(Channels.PlayStation4.xButton)

    override val isLeftBumperPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.PlayStation4.leftBumper)

    override val isRightBumperPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.PlayStation4.rightBumper)

    override val leftTrigger: Double
        get() = deadZone(wpiDevice.getRawAxis(Channels.PlayStation4.leftTrigger), Channels.PlayStation4.deadZone)

    override val rightTrigger: Double
        get() = deadZone(wpiDevice.getRawAxis(Channels.PlayStation4.rightTrigger), Channels.PlayStation4.deadZone)

    override var rumble: Double = 0.0
        set(value) {
            wpiDevice.setRumble(GenericHID.RumbleType.kRightRumble, value)
            wpiDevice.setRumble(GenericHID.RumbleType.kLeftRumble, value)
        }
}
