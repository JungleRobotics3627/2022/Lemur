package jungle3627.lemur.hid.devices.gamepads

import jungle3627.lemur.hid.devices.JungleHID

abstract class JungleGamepad(port: Int) : JungleHID(port) {

    abstract val leftJoystickX: Double

    abstract val leftJoystickY: Double

    abstract val isLeftJoystickPressed: Boolean

    abstract val rightJoystickX: Double

    abstract val rightJoystickY: Double

    abstract val isRightJoystickPressed: Boolean

    abstract val dPad: Int

    /**
     * Y on Xbox
     * Triangle on PS4
     */
    abstract val isTopButtonPressed: Boolean
    abstract val isTopButtonDown: Boolean

    /**
     * B on Xbox
     * Circle on PS4
     */
    abstract val isRightButtonPressed: Boolean
    abstract val isRightButtonDown: Boolean

    /**
     * A on Xbox
     * X on PS4
     */
    abstract val isBottomButtonPressed: Boolean
    abstract val isBottomButtonDown: Boolean

    /**
     * X on Xbox
     * Square on PS4
     */
    abstract val isLeftButtonPressed: Boolean
    abstract val isLeftButtonDown: Boolean

    abstract val isLeftBumperPressed: Boolean

    abstract val isRightBumperPressed: Boolean

    /**
     * In range `0.0` until `1.0`
     */
    abstract val leftTrigger: Double

    /**
     * In range `0.0` until `1.0`
     */
    abstract val rightTrigger: Double

    abstract var rumble: Double
}
