package jungle3627.lemur.hid.devices.gamepads

import edu.wpi.first.wpilibj.GenericHID
import jungle3627.lemur.Constants.InputParameters.Channels

class JungleXbox(port: Int) : JungleGamepad(port) {
    override val leftJoystickX: Double
        get() = deadZone(wpiDevice.getRawAxis(Channels.Xbox360.LeftJoystick.xAxis), Channels.Xbox360.deadZone)

    override val leftJoystickY: Double
        get() = deadZone(-wpiDevice.getRawAxis(Channels.Xbox360.LeftJoystick.yAxis), Channels.Xbox360.deadZone)

    override val isLeftJoystickPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.Xbox360.LeftJoystick.button)

    override val rightJoystickX: Double
        get() = deadZone(wpiDevice.getRawAxis(Channels.Xbox360.RightJoystick.xAxis), Channels.Xbox360.deadZone)

    override val rightJoystickY: Double
        get() = deadZone(-wpiDevice.getRawAxis(Channels.Xbox360.RightJoystick.yAxis), Channels.Xbox360.deadZone)

    override val isRightJoystickPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.Xbox360.RightJoystick.button)

    override val dPad: Int
        get() = wpiDevice.getPOV()

    override val isTopButtonPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.Xbox360.yButton)
    override val isTopButtonDown: Boolean
        get() = wpiDevice.getRawButton(Channels.Xbox360.yButton)

    override val isRightButtonPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.Xbox360.bButton)
    override val isRightButtonDown: Boolean
        get() = wpiDevice.getRawButton(Channels.Xbox360.bButton)

    override val isBottomButtonPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.Xbox360.aButton)
    override val isBottomButtonDown: Boolean
        get() = wpiDevice.getRawButton(Channels.Xbox360.aButton)

    override val isLeftButtonPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.Xbox360.xButton)
    override val isLeftButtonDown: Boolean
        get() = wpiDevice.getRawButton(Channels.Xbox360.xButton)

    override val isLeftBumperPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.Xbox360.leftBumper)

    override val isRightBumperPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.Xbox360.rightBumper)

    override val leftTrigger: Double
        get() = deadZone(wpiDevice.getRawAxis(Channels.Xbox360.leftTrigger), Channels.Xbox360.deadZone)
        
    override val rightTrigger: Double
        get() = deadZone(wpiDevice.getRawAxis(Channels.Xbox360.rightTrigger), Channels.Xbox360.deadZone)

    override var rumble: Double = 0.0
        set(value) {
            wpiDevice.setRumble(GenericHID.RumbleType.kRightRumble, value)
            wpiDevice.setRumble(GenericHID.RumbleType.kLeftRumble, value)
        }
}
