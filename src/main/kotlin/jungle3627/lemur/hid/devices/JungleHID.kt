package jungle3627.lemur.hid.devices

import edu.wpi.first.wpilibj.GenericHID

/**
 * Abstraction of all the controllers that provides one public interface for getting inputs that work with any human
 * interface device setup
 */
abstract class JungleHID(port: Int) {
    /**
     * The WPILib HID for this device
     */
    protected val wpiDevice: GenericHID = GenericHID(port)

    protected fun deadZone(value: Double, zone: Double): Double {
        return if(value < zone && value > -zone)
            0.0
        else
            value
    }
}
