package jungle3627.lemur.hid.devices.joysticks

import jungle3627.lemur.hid.devices.JungleHID

abstract class JungleJoystick(port: Int) : JungleHID(port) {
    abstract val x: Double

    abstract val y: Double

    abstract val isUpPressed: Boolean

    abstract val isDownPressed: Boolean
}