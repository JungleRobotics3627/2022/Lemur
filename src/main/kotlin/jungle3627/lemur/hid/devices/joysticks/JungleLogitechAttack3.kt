package jungle3627.lemur.hid.devices.joysticks

import jungle3627.lemur.Constants.InputParameters.Channels

class JungleLogitechAttack3(port: Int) : JungleJoystick(port) {
    override val x: Double
        // for some reason, the x-axis is inverted on the joystick
        get() = -wpiDevice.getRawAxis(Channels.LogitechAttack3.xAxis)

    override val y: Double
        get() = wpiDevice.getRawAxis(Channels.LogitechAttack3.yAxis)

    override val isUpPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.LogitechAttack3.upButton)

    override val isDownPressed: Boolean
        get() = wpiDevice.getRawButtonPressed(Channels.LogitechAttack3.downButton)
}
