package jungle3627.lemur.hid.setups

import jungle3627.lemur.hid.devices.joysticks.JungleJoystick

data class DoubleJoystick(
    val leftJoystick: JungleJoystick,
    val rightJoystick: JungleJoystick,
) : HIDSetup {
    override val turn: Double
        get() = (leftJoystick.y - rightJoystick.y) / 2

    override val forward: Double
        get() = (leftJoystick.y + rightJoystick.y) / 2

    override val climb: Double
        get() = biggestClimb

    private val biggestClimb: Double
        get() {
            return 0.0
        }

    override val upperConveyor: Double
        get() = 0.0

    override val lowerConveyor: Double
        get() = 0.0

    override val isUpShiftPressed: Boolean
        get() = leftJoystick.isUpPressed || rightJoystick.isUpPressed

    override val isDownShiftPressed: Boolean
        get() = leftJoystick.isDownPressed || rightJoystick.isDownPressed

    override val isFrontHooksPressed: Boolean
        get() = false

    override val isRearHooksPressed: Boolean
        get() = false

    override val isClimbEnableDown: Boolean
        get() = false

    override val isClimbEnableUp: Boolean
        get() = false

    override var rumble: Double
        get() = 0.0
        set(value) { }
}
