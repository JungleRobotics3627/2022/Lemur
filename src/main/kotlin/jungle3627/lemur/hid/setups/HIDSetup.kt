package jungle3627.lemur.hid.setups

import edu.wpi.first.wpilibj.GenericHID

interface HIDSetup {
    /**
     * `x` component of the input vector (the raw turn input)
     */
    val turn: Double

    /**
     * `y` component of the input vector (the raw throttle input)
     */
    val forward: Double

    /**
     * climb axis (might return -1 or 1 based on buttons depending on setup)
     */
    val climb: Double

    /**
     * returns -1 to 1 based on the setups mapping of the upper conveyor control
     */
    val upperConveyor: Double

    /**
     * returns -1 to 1 based on the setups mapping of the lower conveyor control
     */
    val lowerConveyor: Double

    /**
     * Checks if the upShift button has been pressed
     *
     * @see GenericHID.getRawButtonPressed
     */
    val isUpShiftPressed: Boolean

    /**
     * Checks if the downShift button has been pressed
     *
     * @see GenericHID.getRawButtonPressed
     */
    val isDownShiftPressed: Boolean

    /**
     * Checks if front hooks button has been pressed
     */
    val isFrontHooksPressed: Boolean

    /**
     * Checks if back hooks button has been pressed
     */
    val isRearHooksPressed: Boolean

    /**
     * Checks if climb enable condition is down
     */
    val isClimbEnableDown: Boolean

    /**
     * Checks if climb enable condition is up
     */
    val isClimbEnableUp: Boolean

    var rumble: Double
}
