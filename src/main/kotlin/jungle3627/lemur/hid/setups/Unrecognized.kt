package jungle3627.lemur.hid.setups

/**
 * Empty implementation of JungleHID used when no known controller setup is being used
 *
 * All controls return only default values (`0, false, etc.`)
 */
class Unrecognized : HIDSetup {
    override val turn: Double
        get() = 0.0

    override val forward: Double
        get() = 0.0

    override val climb: Double
        get() = 0.0

    override val upperConveyor: Double
        get() = 0.0

    override val lowerConveyor: Double
        get() = 0.0

    override val isUpShiftPressed: Boolean
        get() = false

    override val isDownShiftPressed: Boolean
        get() = false

    override val isFrontHooksPressed: Boolean
        get() = false

    override val isRearHooksPressed: Boolean
        get() = false

    override val isClimbEnableDown: Boolean
        get() = false

    override val isClimbEnableUp: Boolean
        get() = false

    override var rumble: Double
        get() {
            return 0.0
        }
        set(value) { }
}