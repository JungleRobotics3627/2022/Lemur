package jungle3627.lemur.hid.setups

import jungle3627.lemur.hid.devices.joysticks.JungleJoystick

data class SingleJoystick(
    val joystick: JungleJoystick,
) : HIDSetup {
    override val turn: Double
        get() = joystick.x

    override val forward: Double
        get() = joystick.y

    override val climb: Double
        get() = 0.0

    override val upperConveyor: Double
        get() = 0.0

    override val lowerConveyor: Double
        get() = 0.0

    override val isUpShiftPressed: Boolean
        get() = joystick.isUpPressed

    override val isDownShiftPressed: Boolean
        get() = joystick.isDownPressed

    override val isFrontHooksPressed: Boolean
        get() = false

    override val isRearHooksPressed: Boolean
        get() = false

    override val isClimbEnableDown: Boolean
        get() = false

    override val isClimbEnableUp: Boolean
        get() = false

    override var rumble: Double
        get() {
            return 0.0
        }
        set(value) { }
}