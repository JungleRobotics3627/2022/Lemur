package jungle3627.lemur.hid.setups

import jungle3627.lemur.hid.devices.gamepads.JungleGamepad
import kotlin.math.abs

data class SingleGamepad(
    val gamepad: JungleGamepad,
) : HIDSetup {
    override val turn: Double
        get() = gamepad.leftJoystickX

    override val forward: Double
        get() = gamepad.leftJoystickY

    override val upperConveyor: Double
        get() {
            return if(gamepad.isBottomButtonDown) {
                -gamepad.rightTrigger
            } else {
                gamepad.rightTrigger
            }
        }

    override val lowerConveyor: Double
        get() {
            return if(gamepad.isBottomButtonDown) {
                -gamepad.leftTrigger
            } else {
                gamepad.leftTrigger
            }
        }

    override val climb: Double
        get() = gamepad.rightJoystickY

    override val isUpShiftPressed: Boolean
        get() = gamepad.isRightBumperPressed

    override val isDownShiftPressed: Boolean
        get() = gamepad.isLeftBumperPressed

    override val isFrontHooksPressed: Boolean
        get() = gamepad.isRightButtonPressed

    override val isRearHooksPressed: Boolean
        get() = gamepad.isLeftButtonPressed

    override val isClimbEnableDown: Boolean
        get() = (gamepad.rightJoystickX < -0.45 && gamepad.rightJoystickY < -0.45)

    override val isClimbEnableUp: Boolean
        get() = (abs(gamepad.rightJoystickX) < 0.05 && abs(gamepad.rightJoystickY) < 0.05)

    override var rumble: Double
        get() {
            return gamepad.rumble
        }
        set(value) {
            gamepad.rumble = value
        }
}

