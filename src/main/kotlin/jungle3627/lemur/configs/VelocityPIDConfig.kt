package jungle3627.lemur.configs

data class VelocityPIDConfig(
    val kp: Double = 0.0,
    val ki: Double = 0.0,
    val kd: Double = 0.0,
    val kIntegrationRange: Double = 0.0,
    val ks: Double = 0.0,
    val kv: Double = 0.0,
    val ka: Double = 0.0
)