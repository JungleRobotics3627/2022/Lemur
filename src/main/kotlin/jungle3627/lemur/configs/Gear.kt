package jungle3627.lemur.configs

data class Gear(
    val throttleMultiplier: Double,
    val turnMultiplier: Double,
)