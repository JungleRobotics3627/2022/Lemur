package jungle3627.lemur

import edu.wpi.first.wpilibj.TimedRobot
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.CommandScheduler
import jungle3627.lemur.commands.*
import jungle3627.lemur.subsystems.ClimbSubsystem
import jungle3627.lemur.subsystems.ConveyorSubsystem
import jungle3627.lemur.subsystems.DriveSubsystem

import jungle3627.lemur.subsystems.InputSubsystem

/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
class Robot : TimedRobot() {
    private var teleopDriveCommand = TeleopDriveCommand()
    private var teleopClimbCommand = TeleopClimbCommand()
    private var teleopConveyorCommand = TeleopConveyorCommand()

    /**
     * This function is run when the robot is first started up and should be used for any
     * initialization code.
     */
    override fun robotInit() {
        SmartDashboard.putBoolean("Conveyor Auto", false)
        SmartDashboard.putBoolean("Drive Auto", false)

        DriveSubsystem.defaultCommand = teleopDriveCommand
        ConveyorSubsystem.defaultCommand = teleopConveyorCommand
        ClimbSubsystem.defaultCommand = teleopClimbCommand

        InputSubsystem.configure()

        println("robotInit()")
    }

    /**
     * This function is called every robot packet, no matter the mode. Use this for items like
     * diagnostics that you want ran during disabled, autonomous, teleoperated and test.
     *
     *
     * This runs after the mode specific periodic functions, but before
     * LiveWindow and SmartDashboard integrated updating.
     */
    override fun robotPeriodic() {
        // Runs the Scheduler.  This is responsible for polling buttons, adding newly-scheduled
        // commands, running already-scheduled commands, removing finished or interrupted commands,
        // and running subsystem periodic() methods.  This must be called from the robot's periodic
        // block in order for anything in the Command-based framework to work.
        CommandScheduler.getInstance().run()
    }

    /**
     * This function is called once each time the robot enters Disabled mode.
     */
    override fun disabledInit() { }

    /**
     * This function is called periodically when disabled.
     */
    override fun disabledPeriodic() { }

    /**
     * This function is called once when autonomous is enabled.
     */
    override fun autonomousInit() {
        AutoCommand(
            SmartDashboard.getBoolean("Conveyor Auto", false),
            SmartDashboard.getBoolean("Drive Auto", false)
        ).schedule()
    }

    /**
     * This function is called periodically during autonomous.
     */
    override fun autonomousPeriodic() { }

    /**
     * This function is called once when teleop is enabled.
     */
    override fun teleopInit() {
        println("teleopInit()")
        InputSubsystem.configure()
    }

    /**
     * This function is called periodically during operator control.
     */
    override fun teleopPeriodic() { }

    /**
     * This function is called once when test mode is enabled.
     */
    override fun testInit() {
        // Cancels all running commands at the start of test mode.
        CommandScheduler.getInstance().cancelAll()
    }

    /**
     * This function is called periodically during test mode.
     */
    override fun testPeriodic() { }
}
