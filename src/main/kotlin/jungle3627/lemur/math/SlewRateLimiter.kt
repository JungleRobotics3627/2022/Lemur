package jungle3627.lemur.math

import edu.wpi.first.util.WPIUtilJNI.now
import kotlin.math.absoluteValue
import kotlin.math.withSign

/**
 * A simple slew limiter that has a variable rate
 */
class SlewRateLimiter(private var rate: Double, initialValue: Double = 0.0) {
    private var lastValue: Double = initialValue
    private var lastCompute: Double = seconds

    private val seconds: Double
        get() = now() * 1e-6

    fun setRate(rate: Double) {
        this.rate = rate
    }

    fun compute(value: Double): Double {
        val currentTime = seconds
        val deltaTime = currentTime - lastCompute
        val deltaValue = value - lastValue

        val slope = deltaValue / deltaTime

        val output = if (slope.absoluteValue > rate) lastValue + (rate * deltaTime).withSign(slope) else value

        lastValue = output
        lastCompute = currentTime

        return output
    }
}